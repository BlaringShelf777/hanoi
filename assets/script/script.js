
/*
    Hanoi

    1. Verificar disco escolhido pelo usuário
        1.1. verifica se um disco é válido (Disco no topo)
            1.1.1. Se sim, verifica se um disco já foi escolhido
                1.1.1.1. Se sim, troca disco selecionado conforme passo 1.1
                1.1.1.2. Se não, seleciona disco
            1.1.2. Se não, não faz nada

    2. Verificar coluna escolhida pelo usuário
        2.1. Verifica se uma coluna é válida (só há discos menores
        que o disco atual na coluna)
            2.1.1. Se sim, move o disco até a coluna
            2.1.2. Se não, o disco atual é desselecionado e voltamos ao passo 1
    
    3. Verifica fim de jogo (Todas os discos movidos com sucesso até a ultima coluna)
        3.1. Se sim, aviso de fim de jogo e restart
        3.2. Se não, volta ao passo 1

 */

// Edinaldo

const container_game = document.getElementById('container_game')
const box1 = document.getElementById('box1')
const box2 = document.getElementById("box2")
const box3 = document.getElementById("box3")

const start = document.createElement("div")
start.classList.add('varetas', 'varetas--dark')
start.setAttribute('id', 'start')
box1.appendChild(start)
 
const offset = document.createElement("div")
offset.classList.add('varetas', 'varetas--dark')
offset.setAttribute('id', 'offset')
box2.appendChild(offset)

const final = document.createElement("div")
final.classList.add('varetas', 'varetas--dark')
final.setAttribute('id', 'final')
box3.appendChild(final)

let disco_a = document.createElement("div")
disco_a.setAttribute('id', 'disco_a')
disco_a.classList.add('disk')
disco_a.setAttribute('data-val', 3)
box1.appendChild(disco_a)

let disco_b = document.createElement("div")
disco_b.setAttribute('id', 'disco_b')
disco_b.classList.add('disk')
disco_b.setAttribute('data-val', 2)
box1.appendChild(disco_b)

let disco_c = document.createElement("div")
disco_c.setAttribute('id', 'disco_c')
disco_c.classList.add('disk')
disco_c.setAttribute('data-val', 1)
box1.appendChild(disco_c)

let disco_d = document.createElement("div")
disco_d.setAttribute('id', 'disco_d')
disco_d.classList.add('disk')
disco_d.setAttribute('data-val', 0)
box1.appendChild(disco_d)


/* 
    Timer 
*/
// Lucas
let min = 0, sec = 0, hour = 0

const format_num = n => n < 10 ? `0${n}` : n
const timer = _ => {
    const timer_area = document.querySelector('p.timer')
    min = sec = hour = 0

    const id = setInterval( _ => {    
        if (++sec === 60) {
            min += 1
            sec = 0
        }
    
        if (min === 60) {
            min = 0
            hour += 1
        }
        timer_area.innerText = `${format_num(hour)}:${format_num(min)}:${format_num(sec)}`
    }, 1000)

    return id
}

// Lucas

let last_disk = null, is_valid_disk = false, you_won = false, timer_id = null
let best_times = []
const column_area = document.querySelector('section.container_game')
const scores_area = document.querySelector('section.score_area')

const calc_time = obj => {
    const time = obj.time.split(':')

    return time[0] * 3600 + time[1] * 60 + time[2]
}

column_area.addEventListener('click', evt => {
    if (!you_won) {
        const evt_tgt = evt.target
        const evt_parent = evt_tgt.closest('div.column_area')
        let check_end = false
        
        // Disk Click
        if (evt_tgt.dataset.val) {
            if (evt_tgt !== last_disk) {
                if (last_disk) {
                    last_disk.classList.remove('selected')
                }
                is_valid_disk = Object.values(evt_parent.children)
                                .filter(disk => !disk.classList.contains('varetas'))
                                .every(disk => disk.dataset.val >= evt_tgt.dataset.val)
                if (is_valid_disk) {
                    evt_tgt.classList.add('selected')
                    last_disk = evt_tgt
                    if (timer_id === null) timer_id = timer()
                } else {
                    last_disk = null
                }
            } else {
                evt_tgt.classList.remove('selected')
                last_disk = null
            }
        }

        // Column Click
        else if (evt_tgt.classList.contains('varetas')) {
            if (last_disk && is_valid_disk) {
                const disk_col = last_disk.closest('div.column_area').children[0]
                if (disk_col !== evt_tgt) {
                    const col_disks = Object.values(evt_parent.children)
                                      .filter(disk => !disk.classList.contains('varetas'))
                    const is_valid_col = col_disks.length === 0 || col_disks.every(disk => disk.dataset.val >= last_disk.dataset.val)
                                     
                    if (is_valid_col) {
                        const count_area = document.querySelector('span.moves__count')

                        check_end = true
                        evt_parent.appendChild(last_disk)
                        count_area.innerText = Number(count_area.innerText) + 1
                    }
                    last_disk.classList.remove('selected')
                    last_disk = null
                } else {
                    last_disk.classList.remove('selected')
                    last_disk = null
                }
            }
        }

        // Blank Click
        else if (last_disk){
            last_disk.classList.remove('selected')
            last_disk = null
        }

        // Checks End
        if (check_end) {
            const last_col = document.querySelector('div#box3')
            
            if (last_col.children.length - 1 === 4) {
                const confetti_area = document.querySelector('div.confetti')
                const time = document.querySelector('p.timer').innerText
                const moves = Number(document.querySelector('span.moves__count').innerText)

                best_times.push({time, moves})
                best_times.sort((a, b) => calc_time(a) - calc_time(b))
                best_times = best_times.slice(0, 5)
                confetti_area.classList.remove('hidden')
                clearTimeout(timer_id)
                timer_id = null
                setTimeout(reset, 5000)
            }
        }
    }
})

/*
    Reset

    coloca os discos na primeira coluna
*/
// Lucas

const reset = _ => {
    const confetti_area = document.querySelector('div.confetti')
    const cols = Object.values(document.querySelectorAll('div.column_area'))
    const col_area = document.getElementById('box1')
    const timer_area = document.querySelector('p.timer')
    const count_area = document.querySelector('span.moves__count')
    let disks = []

    confetti_area.classList.add('hidden')
    cols.forEach(col => {
        disks = [...disks, ...Object.values(col.children)]
    })

    disks = disks.filter(disk => !disk.classList.contains('varetas'))
    disks.sort((a, b) => b.dataset.val - a.dataset.val)
    disks.forEach(disk => col_area.append(disk))
    if (timer_id !== null) {
        clearTimeout(timer_id)
        timer_id = null
    }
    timer_area.innerText = '00:00:00'
    count_area.innerText = 0
    if (best_times.length) {
        scores_area.innerHTML = ''
        best_times.forEach((obj, index) => {
            const time = document.createElement('p')

            time.classList.add('score_area__item')
            time.innerText = `${index + 1}# ${obj.time} - ${format_num(obj.moves)} moves`
            scores_area.appendChild(time)
        })
    }
}

const reset_button = document.querySelector('button.reset')

reset_button.addEventListener('click', reset)

// Scores 
const scores_button = document.querySelector('button.top_score')

scores_area.addEventListener('click', _ => {
    scores_area.classList.toggle('hidden')
})

scores_button.addEventListener('click', _ => {
    scores_area.classList.toggle('hidden')
})

// dark/ligth mode

const checkbox = document.querySelector('input#mode')

checkbox.addEventListener('change', _ => {
    const container = document.querySelector('div.container')
    const columns = document.querySelectorAll('div.varetas')
    const timer = document.querySelector('p.timer')
    const moves = document.querySelector('p.moves')
    const body = document.querySelector('body')

    container.classList.toggle('container--dark')
    timer.classList.toggle('timer--dark')
    moves.classList.toggle('moves--dark')
    columns.forEach(col => col.classList.toggle('varetas--dark'))
    body.classList.toggle('light')

    console.log(body.children)
})